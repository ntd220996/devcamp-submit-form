import 'bootstrap/dist/css/bootstrap.min.css'

import FormComponents from "./component/form/FormComponent ";
import TitleComponents from "./component/title/TittleComponent ";


function App() {
  return (
    <div className='container' style={{'width': '60%','background-color': 'antiquewhite'}}>
      <>
        <TitleComponents />
        <FormComponents />
      </>
    </div>
  );
}

export default App;
