import { Component } from "react";

class FormComponents extends Component {
    // Khai báo các hàm
    onFirstnameChange(event) {
        let vValueFirstname = event.target.value;
        console.log('Fistname: ' + vValueFirstname);
    }
    onLastnameChange(event) {
        let vValueLastname = event.target.value;
        console.log('Lastname: ' + vValueLastname);
    }
    onCountryChange(event) {
        let vValueCountry = event.target.value;
        console.log('Country: ' + vValueCountry);
    }
    onSubjectChange(event) {
        let vValueSubject = event.target.value;
        console.log('Subject: ' + vValueSubject);
    }
    onSendDataSubmit(e) {
        console.log('Form đã được submit');
        e.preventDefault();
    }


    render() {
        return (
            <>
                <form onSubmit={this.onSendDataSubmit}>
                    <div className="row mb-2">
                        <div className="col-sm-3">
                            <label className="col-form-label">First name</label>
                        </div>
                        <div className="col-sm-9">
                            <input type='text' className="form-control" onChange={this.onFirstnameChange} />
                        </div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-sm-3">
                            <label className="col-form-label" >Last name</label>
                        </div>
                        <div className="col-sm-9">
                            <input type='text' className="form-control" onChange={this.onLastnameChange} />
                        </div>
                    </div>
                    <div className="row mb-2">
                        <div className="col-sm-3">
                            <label className="col-form-label">Country</label>
                        </div>
                        <div className="col-sm-9">
                            <select className="form-select" onChange={this.onCountryChange}>
                                <option value="">Chọn</option>
                                <option value="Viet Nam">Việt Nam</option>
                                <option value="USA">American</option>
                                <option value="Japan">Nhật bản</option>
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3">
                            <label className="col-form-label">Subject</label>
                        </div>
                        <div className="col-sm-9">
                            <textarea className="form-control" name="" id="" onChange={this.onSubjectChange} />
                        </div>
                    </div>
                    
                    <button className="btn btn-success" >Send Data</button>
                </form>
                <br />
            </>
        )
    }
}

export default FormComponents